#!/bin/bash

IMAGE='catalina-client-image.iso'

if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
echo "Verifying that pv is installed..."
# apt-get update > /dev/null && apt-get install pv > /dev/null
clear
echo "Select USB device to flash:"
echo "==========================="
ls -la /dev/disk/by-id/*usb* | awk '{print "\033[97;42m"$9"\033[0m->\033[97;44m"$11"\033[0m\n"}'
read -p "Enter block name (without /dev/ prefix - e.g., sdb): " block
umount /dev/$block?
clear
echo -e "\e[1;97;41m =========================== "
echo -e "\e[1;97;41m |        WARNING!         | "
echo -e "\e[1;97;41m =========================== \e[0m"
echo -e " Proceeding will erase \e[1;4mALL\e[0m"
echo " data on /dev/$block."
echo ""
echo " Ctrl-C to cancel or"
read -n1 -r -p " Press any key to continue..." key
clear
echo "Writing image to /dev/$block..."
pv -s `du -sb $IMAGE | awk '{print $1}'` $IMAGE > /dev/$block
echo "Synchronizing USB write operations..."
sync
echo "Bootable USB successfully created. You may remove the USB drive."
